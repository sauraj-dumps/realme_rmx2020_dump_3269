## sys_oplus_mssi_64_cn-user-11-RP1A.200720.011-1607914664672-release-keys
- Manufacturer: realme
- Platform: 
- Codename: RMX2020
- Brand: realme
- Flavor: aosp_RMX2020-userdebug
- Release Version: 11
- Id: RQ3A.211001.001
- Incremental: 1635238210
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/walleye/walleye:8.1.0/OPM1.171019.011/4448085:user/release-keys
- OTA version: 
- Branch: sys_oplus_mssi_64_cn-user-11-RP1A.200720.011-1607914664672-release-keys
- Repo: realme_rmx2020_dump_3269


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
